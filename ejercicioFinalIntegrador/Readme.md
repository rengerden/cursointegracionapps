# Integracion de Aplicaciones con Apache Camel

### ejercicio final Apache Camel

En este proyecto les presento como seria la respuesta al ejercicio propuesto dentro del curso de Apache Camel, se utiliza una base de datos Postgress que se ejecuta con ayuda de docker.

> docker run --ulimit memlock=-1:-1 -it --rm=true --memory-swappiness=0 --name postgresql-camel -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -e POSTGRES_DB=cameldb -p 5432:5432 postgres:latest

- Me falta agregar ciertas funcionalidades, sin embargo el crud de la informacion del archivo ya se realizo, por ejemplo falta la funcionalidad de la bitacora sobre los archivos procesados y la implementacion del componente extra.

 
