package com.curso.integracion.aplicaciones.routes;

import com.curso.integracion.aplicaciones.entity.OpcionesMeses;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class ApiConfiguration.
 */
@Component
public class ApiConfiguration extends RouteBuilder{
  
  JacksonDataFormat unmarshall = new JacksonDataFormat(OpcionesMeses.class);
  /**
   * Configure.
   *
   * @throws Exception the exception
   */
  @Override
  public void configure() throws Exception {
    
    from("rest:get:hello")
    .transform().constant("Hello World");
    
    from("rest:get:opcionesPago:/?produces='application/json'&bindingMode=json")
      .to("{{route.metodosPago.findAll}}");
    
    from("rest:get:opcionesPago:/{id}?produces='application/json'&bindingMode=json")
      .to("{{route.metodosPago.findById}}");   
    
    from("rest:post:opcionesPago:/?produces='application/json'&consumes='application/json'&bindingMode=json")
      .to("{{route.metodosPago.create}}");
    
    from("rest:put:opcionesPago:/?produces='application/json'&consumes='application/json'&bindingMode=json")
        .to("{{route.metodosPago.create}}");
    
    from("rest:delete:opcionesPago:/{id}?produces='application/json'&consumes='application/json'&bindingMode=json")
      .to("{{route.metodosPago.delete}}");
    
    from("rest:get:opcionesPago/:archivos?produces='application/json'&consumes='application/json'&bindingMode=json")
      .to("{{route.metodosPago.archivos.findAll}}");
    
    from("rest:post:opcionesPago/:loadFile?produces='application/json'&consumes='application/json'&bindingMode=json")
      .to("{{route.metodosPago.archivos.loadFile}}");
  }

}
