package com.curso.integracion.aplicaciones.routes;

import com.curso.integracion.aplicaciones.business.OpcionesMesesBusiness;
import com.curso.integracion.aplicaciones.entity.OpcionesMeses;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class MatodosPago.
 */
@Component
public class MetodosPago extends RouteBuilder{
  
  /**
   * Configure.
   *
   * @throws Exception the exception
   */
  @Override
  public void configure() throws Exception {
    
    from("{{route.metodosPago.findAll}}")
      .bean(OpcionesMesesBusiness.class,"findAll")
      .marshal().json(JsonLibrary.Jackson,OpcionesMeses.class)
        .choice().when().simple("${body} == '[]'")
           .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
             .to("{{route.metodosPago.archivos.loadFile}}")
              
           .setBody(simple("${header.cargaArchivoEstatus}"))
    .end();
    
    from("{{route.metodosPago.findById}}")
      .bean(OpcionesMesesBusiness.class,"findOpcionesMesesByopcionId(${header.Id})")
        .marshal().json(JsonLibrary.Jackson,OpcionesMeses.class)
        .choice()
          .when(bodyAs(String.class).not().contains("Fault Code: 0"))
            .setBody(constant(""))
            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
    .end();
    
    from("{{route.metodosPago.create}}")
     .choice().when(header("cargaArchivo").isEqualTo("1"))
        .log("cargaBatch")
        .log("Received Body ${body}")
        .bean(OpcionesMesesBusiness.class,"save(${body})")        
     .otherwise()
        .log("carga API")
      .unmarshal().json(JsonLibrary.Jackson,OpcionesMeses.class)
        .log("Received Body ${body}")
        .setHeader("cargaArchivo", simple("0"))
      .process(this::buildOpcionesPago)
        .bean(OpcionesMesesBusiness.class,"save(${body})")
         .marshal().json(JsonLibrary.Jackson,OpcionesMeses.class)
    .end();
    
    from("{{route.metodosPago.delete}}")
      .bean(OpcionesMesesBusiness.class,"deleteByopcionId(${header.Id})")
        .choice()
          .when(bodyAs(String.class).not().contains("Fault Code: 0"))
            .setBody(constant(""))
            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
          .otherwise()
            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(204))
     .end();
}
  
  /**
   * Builds the opciones pago.
   *
   * @param exchange the exchange
   * @throws Exception the exception
   */
  @SuppressWarnings("static-access")
  private void buildOpcionesPago(Exchange exchange) throws Exception{
    exchange.getIn().setBody( exchange.getIn().getBody(OpcionesMeses.class).builder()
        .cargaArchivo(exchange.getIn().getHeader("cargaArchivo",Integer.class))
        .exchangeId(exchange.getExchangeId())
        .creadoEn(new Date())
        .build());
  } 

}
