package com.curso.integracion.aplicaciones.routes;

import com.curso.integracion.aplicaciones.entity.OpcionesMeses;
import com.curso.integracion.aplicaciones.entity.OpcionesMesesArchivo;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import org.springframework.stereotype.Component;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class CargaArchivos.
 */
@Component
public class CargaArchivos extends RouteBuilder{

  /**
   * Configure.
   *
   * @throws Exception the exception
   */
  @Override
  public void configure() throws Exception {
   
    from("{{route.metodosPago.archivos.loadFile}}")
    .log("cargandoArchivo")
      .pollEnrich("file:/home/rengerden/camelFiles/in?noop=true&fileName=prefijosMeses.csv")
         .unmarshal().bindy(BindyType.Csv,OpcionesMesesArchivo.class)
           .split(body())
             .setHeader("cargaArchivo", simple("1"))
             .process(this::transformaArchivo)
             .log("${body}")
               .to("{{route.metodosPago.create}}")
           .end()
           .log("termino carga")
           .setHeader("cargaArchivoEstatus", simple("inicio carga"))
           .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
           .setBody(constant("ssss"))
           .end();
    
  }
  
  private void transformaArchivo (Exchange exchange) throws Exception {
    OpcionesMesesArchivo opcionesArchivo = exchange.getIn().getBody(OpcionesMesesArchivo.class);
    exchange.getIn().setBody(OpcionesMeses.builder()
        .opcionId(0)
        .prefijoTarjeta(opcionesArchivo.getPrefijo())
        .institutcionBancaria(opcionesArchivo.getInstitucionBancaria())
        .tipoTarjeta(opcionesArchivo.getTipoTarjeta())
        .nombreBanco(opcionesArchivo.getNombreBanco())
        .estatus(opcionesArchivo.getEstatusVenta())
        .tipoPago(String.valueOf(opcionesArchivo.getTipoPago()))
        .monto3Meses(0)
        .monto6Meses(0)
        .monto12Meses(0)
        .creadoEn(new Date())
        .cargaArchivo(exchange.getIn().getHeader("cargaArchivo",Integer.class))
        .exchangeId(exchange.getExchangeId())
        .build());
 
  }
  
  

}
