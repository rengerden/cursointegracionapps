package com.curso.integracion.aplicaciones.service;

import com.curso.integracion.aplicaciones.entity.OpcionesMeses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface OpcionesMesesInterface extends JpaRepository<OpcionesMeses, Integer>{

  public OpcionesMeses findOpcionesMesesByopcionId(int opcionId);

  public void deleteByopcionId(int opcionId);
}
