package com.curso.integracion.aplicaciones.entity;

import lombok.Data;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new archivos procesados.
 */

@Entity(name="archivosProcesados")
@Data
public class ArchivosProcesados {

  /** The id archivo. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int idArchivo; 
  
  /** The nombre. */
  private String nombre;
  
  /** The created at. */
  private Date createdAt; 
  
  /** The registros procesados. */
  private int registrosProcesados; 
  
  /** The exchange id. */
  private String exchangeId; 
}
