package com.curso.integracion.aplicaciones.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public class SimpleBusiness <E, R extends CrudRepository<E , Integer>> implements RepoInterface<E>{

  @Autowired
  protected R repository;
  
  @Override
  public Iterable<E> findAll() {
    return repository.findAll();
  }

  @Override
  public Optional<E> findById(Integer id) {
    return repository.findById(id);
  }

  @Override
  public E save(E e) {
    return repository.save(e);
  }

  @Override
  public void deleteById(Integer id) {
    repository.deleteById(id);    
  }
  
}
