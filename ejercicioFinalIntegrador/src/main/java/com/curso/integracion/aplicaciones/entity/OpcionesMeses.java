package com.curso.integracion.aplicaciones.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new opciones meses.
 */

/**
 * Instantiates a new opciones meses.
 */

@Entity(name="opcionesMeses")
@Data
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpcionesMeses{
  

  /** The opcion id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int opcionId; 
  
  /** The prefijo tarjeta. */
  private int prefijoTarjeta;
  
  /** The institutcion bancaria. */
  private String institutcionBancaria; 
  
  /** The tipo tarjeta. */
  private String tipoTarjeta; 
  
  /** The nombre banco. */
  private String nombreBanco; 
  
  /** The estatus. */
  private int estatus; 
  
  /** The tipo pago. */
  private String tipoPago;  
  
  /** The monto 3 meses. */
  private int monto3Meses; 
  
  /** The monto 6 meses. */
  private int monto6Meses; 
  
  /** The monto 12 meses. */
  private int monto12Meses; 
  
  /** The creado en. */
  private Date creadoEn; 
  
  /** The carga archivo. */
  private int cargaArchivo; 
  
  /** The exchange id. */
  private String exchangeId;

}
