package com.curso.integracion.aplicaciones.business;

import com.curso.integracion.aplicaciones.entity.OpcionesMeses;
import com.curso.integracion.aplicaciones.service.OpcionesMesesInterface;
import com.curso.integracion.aplicaciones.service.SimpleBusiness;

// TODO: Auto-generated Javadoc
/**
 * The Class OpcionesMesesBusiness.
 */
public class OpcionesMesesBusiness extends SimpleBusiness<OpcionesMeses, OpcionesMesesInterface>{
  
  
   /**
    * Find opciones meses byopcion id.
    *
    * @param operationId the operation id
    * @return the opciones meses
    */
   public OpcionesMeses findOpcionesMesesByopcionId(int operationId) {
     return repository.findOpcionesMesesByopcionId(operationId);
   }
   
   /**
    * Delete byopcion id.
    *
    * @param operationId the operation id
    */
   public void deleteByopcionId(int operationId) {
     repository.deleteByopcionId(operationId);
   }
}
