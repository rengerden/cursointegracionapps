package com.curso.integracion.aplicaciones.service;

import com.curso.integracion.aplicaciones.entity.ArchivosProcesados;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface ArchivosProcesadosInteface.
 */
@Repository
public interface ArchivosProcesadosInteface extends JpaRepository<ArchivosProcesados, Integer>{

}
