package com.curso.integracion.aplicaciones.service;

import java.util.Optional;

public interface RepoInterface <E> {
  
  public Iterable<E> findAll();
  
  public Optional<E> findById(Integer id);
  
  public E save(E e);
  
  public void deleteById(Integer id);
   

}
