package com.curso.integracion.aplicaciones.entity;

import lombok.Data;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

// TODO: Auto-generated Javadoc
/**
 * The Class OpcionesMesesArchivo.
 */
@CsvRecord(separator = ",", skipFirstLine = true)

/**
 * Instantiates a new opciones meses archivo.
 */
@Data
public class OpcionesMesesArchivo {

  /** The prefijo. */
  @DataField(pos = 1)
  private int prefijo;

  /** The institucion bancaria. */
  @DataField(pos = 2)
  private String institucionBancaria;

  /** The nombre banco. */
  @DataField(pos = 3)
  private String nombreBanco;

  /** The estatus venta. */
  @DataField(pos = 4)
  private int estatusVenta;

  /** The region id. */
  @DataField(pos = 5)
  private int regionId;

  /** The msi. */
  @DataField(pos = 6)
  private String msi;

  /** The tipo tarjeta. */
  @DataField(pos = 7)
  private String tipoTarjeta;

  /** The tipo pago. */
  @DataField(pos = 8)
  private int tipoPago;
}
