package com.curso.integracion.aplicaciones.apache.camel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class CamelApi extends RouteBuilder{

  @Override
  public void configure() throws Exception {
  
    restConfiguration()
    .contextPath("/rest/*")
    .apiContextPath("/api-doc")
    .apiProperty("api.title", "Spring Boot Camel Postgres Rest API.")
    .apiProperty("api.version", "1.0")
    .apiProperty("cors", "true")
    .apiContextRouteId("doc-api")
    .port("8090")
    .bindingMode(RestBindingMode.json);
    
    rest("/book")
    .consumes(MediaType.APPLICATION_JSON_VALUE)
    .produces(MediaType.APPLICATION_JSON_VALUE)
    .get("/{name}").route()
    .to("direct:findAllBooks")
    .endRest()
    ;
    
    from("direct:findAllBooks")
    .log("ok");
  }

  
  
}
