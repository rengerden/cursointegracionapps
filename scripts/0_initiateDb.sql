CREATE SCHEMA IF NOT EXISTS integrator DEFAULT CHARACTER SET utf8mb4;
USE integrator;
DROP TABLE IF EXISTS users;

CREATE TABLE users ( 
    userId varchar(20) not null,
    userName varchar(30) not null

);

CREATE TABLE integrator_info (
    id varchar(20) not null,
    createdAt timestamp,
    state varchar(100) not null,
    uploaded number,
    registered int,
    excluded int,
    infoId varchar(100) not null
);
