# Spring-boot-camel-postgresql


Proyecto Springboot que expone apis generadas con Apache Camel y que utiliza el componente de JPA para hacer ciertas consultas a la base de datos de la entidad Libros (Book)

Para correr el proyecto debes ejecutar este comando antes para poder correr postgres usando docker
> docker run --ulimit memlock=-1:-1 -it --rm=true --memory-swappiness=0 --name postgresql-camel -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -e POSTGRES_DB=cameldb -p 5432:5432 postgres:latest